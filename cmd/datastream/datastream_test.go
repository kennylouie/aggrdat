package main

import (
  "testing"
  "net/http"
  "net/http/httptest"
  "encoding/json"
  "bytes"

  "gitlab.com/kennylouie/aggrdat/pkg/datastream"
)

func TestCreateEndPoint(t *testing.T) {

  apiRequest := &datastream.StreamRequest {
    Streamlength: 1,
  }
  jsonApiRequest, _ := json.Marshal(apiRequest)

  request, _ := http.NewRequest("GET", "/streamwearable", bytes.NewBuffer(jsonApiRequest))
  response := httptest.NewRecorder()

  router := datastream.NewRouter()
  router.ServeHTTP(response, request)

  if response.Result().StatusCode != http.StatusOK {
    t.Errorf("Expected status %d but got %d", http.StatusOK, response.Result().StatusCode)
  }
}

