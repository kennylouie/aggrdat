package main

import (
  "log"
  "net/http"

  "gitlab.com/kennylouie/aggrdat/pkg/datastream"
  "github.com/gorilla/handlers"
)

func main() {

  port := "3000"

  router := datastream.NewRouter()

  allowedOrigins := handlers.AllowedOrigins([]string{"*"})
  allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "DELETE", "PUT"})

  log.Fatal(http.ListenAndServe(":" + port, handlers.CORS(allowedOrigins, allowedMethods)(router)))

}