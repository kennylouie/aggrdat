# Aggrdat

*Short for aggregate data or aggregate that*

## Introduction

Aggrdat is a platform that will injest IoT or big data and provide a dashboard populated with summary statistics, machine trained predictions, visualizations.

This will be a template webapp using D3, React, Redux, MongoDB, and Go.

## Progress

2018-08-09:
An exposed API written in Go is able to mock the collection of wearable heartrate data. A GET request can be made to the API with a json request of

```
{"streamlength": int}
```

where streamlength is the length of wearable data to return back. The return body will be a json with an array of

```
{"time": time in RFC3339Nano format, "heartrate": 120 +/- 20}
```

The current heartrate is intended to mimic actively exercising adults at a mean of 120 with a standard deviation of 20. The returned data array will contain heartrate that will follow a normal distribution of heartrates.

e.g.

```bash
curl -X GET -d '{"streamlength": 10}' localhost:3000/streamwearable
```

returns

```
[{"time":"2018-08-09T21:25:50.0633759Z","heartrate":136.1938503931618},{"time":"2018-08-09T21:25:50.1633817Z","heartrate":119.06472541624119},{"time":"2018-08-09T21:25:50.2633874Z","heartrate":127.58846144947233},{"time":"2018-08-09T21:25:50.3633931Z","heartrate":129.37298629667174},{"time":"2018-08-09T21:25:50.4633988Z","heartrate":146.79411561473287},{"time":"2018-08-09T21:25:50.5634045Z","heartrate":142.81858342881392},{"time":"2018-08-09T21:25:50.6634103Z","heartrate":134.77729819801263},{"time":"2018-08-09T21:25:50.763416Z","heartrate":164.91316940839113},{"time":"2018-08-09T21:25:50.8634217Z","heartrate":82.32797301025275},{"time":"2018-08-09T21:25:50.9634274Z","heartrate":107.3391324360305}]
```