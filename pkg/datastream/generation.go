package datastream

import (
  "gitlab.com/kennylouie/aggrdat/pkg/utils"
  "time"
)

func GenerateMeasurement(mean, std float64) Measurement {

  var m = Measurement {
    time.Now().UTC().Format(time.RFC3339Nano),
    utils.RandNorm(mean, std),
  }

  return m
}

func GenerateMeasurementStream(streamLength int, mean, std float64) []Measurement {

  var m []Measurement

  for i := 0; i < streamLength; i++ {
    time.Sleep(100000000 * time.Nanosecond)
    m = append(m, GenerateMeasurement(mean, std))
  }

  return m
}