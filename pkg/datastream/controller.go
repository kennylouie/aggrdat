package datastream

import(
  "net/http"
  "encoding/json"
  "log"
)

type Controller struct{}

func (c *Controller) StreamWearable(w http.ResponseWriter, r *http.Request) {

  w.Header().Set("Content-Type", "application/json; charset=UTF-8")


  decoder := json.NewDecoder(r.Body)
  var streamRequest StreamRequest
  err := decoder.Decode(&streamRequest)
  if err != nil {
    log.Fatalln("Error decoding json body: ", err)
    w.WriteHeader(http.StatusUnprocessableEntity)
  }


  streamPackage := GenerateMeasurementStream(streamRequest.Streamlength, 120, 20)
  response, err := json.Marshal(streamPackage)
  if err != nil {
    log.Fatalln("Error mashalling json response: ", err)
    w.WriteHeader(http.StatusInternalServerError)
    return
  }

  w.WriteHeader(http.StatusOK)
  w.Write(response)
  return

}