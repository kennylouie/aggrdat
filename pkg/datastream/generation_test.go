package datastream

import (
  "testing"
)

var exactGenerateMeasurementTests = []struct {
  mean float64  // input
  std float64   // input
  expected  float64  // expected result of heart rate
}{
  // exact test
  {
    120,
    0, // no std ensures that the heart rate generated result should be 120
    120,
  },
  {
    10,
    0,
    10,
  },
  {
    10000,
    0,
    10000,
  },
}

var rangedGenerateMeasurementTests = []struct {
  mean  float64
  std   float64
  expected  [2]float64
}{
  {
    120,
    0,
    [2]float64{120, 120},
  },
  {
    120,
    10,
    [2]float64{100, 140},
  },
  {
    120,
    50,
    [2]float64{50, 180},
  },
}

var GenerateMeasurementStreamTests = []struct {
  length  int
  mean    float64
  std     float64
  expected  int   // the dimension of the resulting json array
}{
  {
    1,
    120,
    0,
    1,
  },
  {
    10,
    120,
    0,
    10,
  },
  {
    100,
    120,
    0,
    100,
  },
  {
    500,
    120,
    0,
    500,
  },
}

func TestExactGenerateMeasurement(t *testing.T) {
  for _, input := range exactGenerateMeasurementTests {
    actual := GenerateMeasurement(input.mean, input.std)
    if actual.Heartrate != input.expected {
      t.Errorf("GenerateMeasurement(%v, %v): expected %v but got %v", input.mean, input.std, input.expected, actual.Heartrate)
    }
  }
}

func TestRangedGenerateMeasurement(t *testing.T) {
  for _, input := range rangedGenerateMeasurementTests {
    actual := GenerateMeasurement(input.mean, input.std)
    if ( actual.Heartrate < input.expected[0] || actual.Heartrate > input.expected[1] ) {
      t.Errorf("GenerateMeasurement(%v, %v): expected to be between %v and %v, but got %v", input.mean, input.std, input.expected[0], input.expected[1], actual.Heartrate)
    }
  }
}

func TestGenerateMeasurementStream(t *testing.T) {
  for _, input := range GenerateMeasurementStreamTests {
    actual := GenerateMeasurementStream(input.length, input.mean, input.std)
    resultLength := len(actual)
    if resultLength != input.expected {
      t.Errorf("GenerateMeasurementStream(%d, %v, %v): expected length of result to be %v, but got %v.", input.length, input.mean, input.std, input.expected, resultLength)
    }
  }
}
