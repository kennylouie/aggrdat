package datastream

type Measurement struct {
  Time  string   `json:"time"`
  Heartrate float64 `json:"heartrate"`
}

type Measurements []Measurement

type StreamRequest struct {
  Streamlength  int `json:"streamlength"`
}