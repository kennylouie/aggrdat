package utils

import (
  "math/rand"
  "time"
)

// generate random number following the normal distribution with a mean of 0 and standard deviation of 0
func RandNormDefault() float64 {
  rand.Seed(time.Now().UTC().UnixNano())
  return rand.NormFloat64()
}

// generate random number following the normal distribution
func RandNorm(mean, std float64) float64 {
  rand.Seed(time.Now().UTC().UnixNano())
  return rand.NormFloat64() * std + mean
}

// normally distributed array with mean 0 and standard deviation 0
func RandomNormalArrayDefault(len int) []float64 {
  rand.Seed(time.Now().UTC().UnixNano())
  a := make([]float64, len)

  for i := 0; i < len; i++ {
    a[i] = rand.NormFloat64()
  }

  return a
}

// specified mean and standard deviation
func RandomNormalArray(len int, mean, std float64) []float64 {
  rand.Seed(time.Now().UTC().UnixNano())
  a := make([]float64, len)

  for i := 0; i < len; i++ {
    a[i] = rand.NormFloat64() * std + mean
  }

  return a
}

// calculates mean of float64 array
func Mean(array []float64) float64 {
  arrayLength := len(array)

  var m float64
  for i := 0; i < arrayLength; i++ {
    m += array[i]
  }

  m = m / float64(arrayLength)
  return m
}
